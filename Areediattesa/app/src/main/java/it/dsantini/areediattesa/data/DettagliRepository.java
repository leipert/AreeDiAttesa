package it.dsantini.areediattesa.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.AreaEmergenza;

public interface DettagliRepository {
    /***
     * @return Area di emergenza selezionata
     */
    @NonNull
    LiveData<AreaEmergenza> getArea();

    /***
     * @param a Area di emergenza da selezionare
     */
    void setArea(AreaEmergenza a);
}
