package it.dsantini.areediattesa.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Mappa;

public interface MappaRepository {
    /***
     * @return Mappa osservabile
     */
    @NonNull
    LiveData<Mappa> getMappa();

    /***
     * @return Stato osservabile della mappa
     */
    @NonNull
    LiveData<Stato> getStato();

    /***
     * @param m Nuova mappa da impostare
     */
    void setMappa(Mappa m);

    /** Imposta la mappa da un altro thread
     * @param m Nuova mappa da impostare
     */
    void postMappa(Mappa m);

    /***
     * Salva la mappa nella persistenza
     */
    void salvaMappa();
}
