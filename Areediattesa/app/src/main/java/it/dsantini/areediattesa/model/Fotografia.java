package it.dsantini.areediattesa.model;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import java.util.Optional;

public interface Fotografia {
    /**
     * @return Immagine
     */
    @NonNull
    Drawable getImmagine();

    /**
     * @return Titolo della fotografia
     */
    @NonNull
    Optional<String> getTitolo();
}
