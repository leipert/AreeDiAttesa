package it.dsantini.areediattesa.model;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.view.helper.GeometriaPainter;

public interface Geometria {
    /**
     * @return Un Nodo che si trova sulla superficie della Geometria
     */
    @NonNull
    Nodo getPuntoSuSuperficie();

    /**
     * Pattern Visitor per la stampa della geometria (funzione accept)
     * Implementazione tipica: return gp.stampa(this)
     *
     * @param gp Painter con cui stampare la geometria
     */
    void accept(GeometriaPainter gp);
}
