package it.dsantini.areediattesa.model;

public interface Nodo extends Geometria {
    /**
     * @return Latitudine del nodo
     */
    double getLatitudine();

    /**
     * @return Longitudine del nodo
     */
    double getLongitudine();
}
