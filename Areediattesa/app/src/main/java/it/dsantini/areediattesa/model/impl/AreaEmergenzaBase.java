package it.dsantini.areediattesa.model.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Optional;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Fotografia;
import it.dsantini.areediattesa.model.Geometria;
import it.dsantini.areediattesa.model.TipoEmergenza;

public class AreaEmergenzaBase implements AreaEmergenza {
    @NonNull
    private final String nome;
    @Nullable
    private final String descrizione, gestore;
    @NonNull
    private final TipoEmergenza[] emergenze;
    @NonNull
    private final Fotografia[] fotografie;
    @NonNull
    private final Geometria geometria;

    public AreaEmergenzaBase(@NonNull String nome,
                             @Nullable String descrizione,
                             @Nullable String gestore,
                             @NonNull TipoEmergenza[] emergenze,
                             @NonNull Fotografia[] fotografie,
                             @NonNull Geometria geometria) {
        if (nome == null)
            throw new IllegalArgumentException("nome == null");

        if (emergenze == null)
            throw new IllegalArgumentException("emergenze == null");

        for (int i = 0; i < emergenze.length; i++)
            if (emergenze[i] == null)
                throw new IllegalArgumentException("emergenze[" + i + "] == null");

        if (nome == null)
            throw new IllegalArgumentException("fotografie == null");

        for (int i = 0; i < fotografie.length; i++)
            if (fotografie[i] == null)
                throw new IllegalArgumentException("fotografie[" + i + "] == null");

        for (int i = 0; i < fotografie.length; i++)
            for (int j = 0; j < fotografie.length; j++)
                if (i != j && fotografie[i] == fotografie[j])
                    throw new IllegalArgumentException("fotografie[" + i + "] == fotografie[" + j + "]");

        if (geometria == null)
            throw new IllegalArgumentException("geometria == null");

        this.nome = nome;
        this.descrizione = descrizione;
        this.gestore = gestore;
        this.emergenze = emergenze;
        this.fotografie = fotografie;
        this.geometria = geometria;
    }

    @NonNull
    @Override
    public Optional<String> getDescrizione() {
        if (descrizione == null)
            return Optional.empty();
        else
            return Optional.of(descrizione);
    }

    @NonNull
    @Override
    public TipoEmergenza[] getEmergenze() {
        return emergenze;
    }

    @NonNull
    @Override
    public Fotografia[] getFotografie() {
        return fotografie;
    }

    @NonNull
    @Override
    public Geometria getGeometria() {
        return geometria;
    }

    @NonNull
    @Override
    public Optional<String> getGestore() {
        if (gestore == null)
            return Optional.empty();
        else
            return Optional.of(gestore);
    }

    @NonNull
    @Override
    public String getNome() {
        return nome;
    }
}
