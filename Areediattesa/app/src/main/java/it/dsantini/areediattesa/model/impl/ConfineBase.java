package it.dsantini.areediattesa.model.impl;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Optional;

import it.dsantini.areediattesa.model.Confine;

public abstract class ConfineBase implements Confine {
    @Nullable
    private final String nome;

    ConfineBase(@Nullable String nome) {
        this.nome = nome;
    }

    @Override
    @NonNull
    public Optional<String> getNome() {
        return Optional.ofNullable(nome);
    }
}
