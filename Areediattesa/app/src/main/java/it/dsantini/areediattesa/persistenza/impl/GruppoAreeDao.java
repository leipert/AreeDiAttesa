package it.dsantini.areediattesa.persistenza.impl;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

import it.dsantini.areediattesa.model.GruppoAree;

@Dao
public interface GruppoAreeDao {
    @Query("SELECT * FROM GruppoAreeBase")
    GruppoAree[] loadAllGruppi();
}
