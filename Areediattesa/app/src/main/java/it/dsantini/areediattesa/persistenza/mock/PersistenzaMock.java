package it.dsantini.areediattesa.persistenza.mock;

import android.support.annotation.NonNull;

import java.time.LocalDateTime;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Fotografia;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.model.TipoEmergenza;
import it.dsantini.areediattesa.model.impl.AreaAttesa;
import it.dsantini.areediattesa.model.impl.BoundingBoxBase;
import it.dsantini.areediattesa.model.impl.GruppoAreeBase;
import it.dsantini.areediattesa.model.impl.MappaBase;
import it.dsantini.areediattesa.model.impl.NodoBase;
import it.dsantini.areediattesa.model.impl.TipoEmergenzaBase;
import it.dsantini.areediattesa.persistenza.Persistenza;

public class PersistenzaMock implements Persistenza {

    @Override
    public void salva(@NonNull Mappa m) {

    }

    @NonNull
    @Override
    public Mappa carica() {
        // Dati da http://overpass-turbo.eu/s/Ggf
        TipoEmergenza[] emergenze = new TipoEmergenza[]{new TipoEmergenzaBase("Terremoto")};
        AreaEmergenza[] aree = new AreaEmergenza[]{
                new AreaAttesa("Area verde Forum Zolino",
                        "Area di attesa \"Area verde Forum Zolino\"",
                        "Comune di Imola",
                        emergenze,
                        new Fotografia[0],
                        new NodoBase(44.3656186, 11.6947958),
                        null,
                        false),
                new AreaAttesa("Parcheggio retro cimitero Piratello",
                        null,
                        "Comune di Imola",
                        emergenze,
                        new Fotografia[0],
                        new NodoBase(44.367045, 11.667867),
                        null,
                        false)
        };
        GruppoAree a = new GruppoAreeBase(aree,
                new BoundingBoxBase(44.3564, 44.3786, 11.6677, 11.6975, "Quartiere Zolino Imola"),
                true,
                LocalDateTime.now());
        return new MappaBase(new GruppoAree[]{a});
    }
}
