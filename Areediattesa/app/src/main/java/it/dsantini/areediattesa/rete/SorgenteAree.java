package it.dsantini.areediattesa.rete;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.BoundingBox;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;

public interface SorgenteAree {
    /**
     * @param c Confine di cui scaricare le aree
     * @return Gruppo di aree con il confine specificato
     */
    @NonNull
    GruppoAree download(@NonNull Confine c) throws DownloadException;

    /**
     * Pattern Visitor per download gruppo di aree dato il confine (funzione visit)
     *
     * @param b Confine di cui scaricare le aree
     * @return Gruppo di aree con il confine specificato
     */
    @NonNull
    GruppoAree visit(@NonNull BoundingBox b) throws DownloadException;
}
