package it.dsantini.areediattesa.rete.impl;

import android.support.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class SorgenteAreeRetrofit extends SorgenteAreeBase {
    @NonNull
    private final String endpoint;

    SorgenteAreeRetrofit(@NonNull String endpoint) {
        if (endpoint == null)
            throw new IllegalArgumentException("endpoint null");

        if (endpoint.isEmpty())
            throw new IllegalArgumentException("endpoint vuoto");

        this.endpoint = endpoint;
    }

    protected Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(endpoint)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
