package it.dsantini.areediattesa.rete.mock;

import android.support.annotation.NonNull;

import java.time.LocalDateTime;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.BoundingBox;
import it.dsantini.areediattesa.model.Fotografia;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.TipoEmergenza;
import it.dsantini.areediattesa.model.impl.AreaAttesa;
import it.dsantini.areediattesa.model.impl.GruppoAreeBase;
import it.dsantini.areediattesa.model.impl.NodoBase;
import it.dsantini.areediattesa.model.impl.TipoEmergenzaBase;
import it.dsantini.areediattesa.rete.impl.SorgenteAreeBase;

public class SorgenteAreeMock extends SorgenteAreeBase {
    @Override
    @NonNull
    public GruppoAree visit(@NonNull BoundingBox b) {
        if (b == null)
            throw new IllegalArgumentException("b == null");

        AreaEmergenza area = new AreaAttesa("Area emergenza mock",
                "Descrizione mock",
                "Gestore mock",
                new TipoEmergenza[]{new TipoEmergenzaBase("Terremoto")},
                new Fotografia[0],
                new NodoBase((b.getLatMin() + b.getLatMax()) / 2, (b.getLonMin() + b.getLonMax()) / 2),
                100,
                false);

        return new GruppoAreeBase(new AreaEmergenza[]{area}, b, false, LocalDateTime.now());
    }
}
