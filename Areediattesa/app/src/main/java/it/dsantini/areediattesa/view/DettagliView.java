package it.dsantini.areediattesa.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Optional;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.databinding.ActivityDettagliViewBinding;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Fotografia;
import it.dsantini.areediattesa.model.TipoEmergenza;
import it.dsantini.areediattesa.viewModel.StatoDettagli;
import it.dsantini.areediattesa.viewModel.impl.DettagliViewModelBase;

public class DettagliView extends AppCompatActivity implements Observer<AreaEmergenza> {
    private static final String CLASS = "DettagliView";

    private DettagliViewModelBase viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(DettagliViewModelBase.class);

        setContentView(R.layout.activity_dettagli_view);

        ActivityDettagliViewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_dettagli_view);
        binding.setViewModel(viewModel);

        viewModel.getArea().observe(this, this);
        viewModel.getStato().observe(this, new StatoDettagliObserver());
    }

    private class StatoDettagliObserver implements Observer<StatoDettagli> {
        @Override
        public void onChanged(@Nullable StatoDettagli stato) {
            final String TAG = "DettagliView::onChanged(StatoDettagli)";

            if (stato == null)
                Log.w(TAG, "Stato nullo");
            else if (stato == StatoDettagli.pronto) {
                Log.v(TAG, "Pronto");
            } else {
                try {
                    int id = getResources().getIdentifier(stato.name(), "string", getPackageName());
                    Snackbar.make(findViewById(R.id.dettagli), getString(id), Snackbar.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.e(TAG, "Errore nella visualizzazione di " + stato.toString(), e);
                    Snackbar.make(findViewById(R.id.dettagli), getString(R.string.possibile_errore_dettagli), Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onChanged(@Nullable AreaEmergenza area) {
        final String TAG = "DettagliView::onChanged(AreaEmergenza)";

        if (area == null) {
            Log.e(TAG, "Area da visualizzare nulla");
            Snackbar.make(findViewById(R.id.dettagli), getString(R.string.impossibile_visualizzare_area), Snackbar.LENGTH_INDEFINITE).show();
        } else {
            stampaArea(area);
        }
    }

    protected void stampaArea(@NonNull AreaEmergenza area) {
        final String TAG = "DettagliView::stampaArea";

        if (area.getNome().trim().isEmpty()) {
            TextView nomeView = findViewById(R.id.nomeArea);
            nomeView.setText(getString(R.string.nomeAssente));
        }

        Optional<String> optDesc = area.getDescrizione(),
                optGestore = area.getGestore();

        TextView desc = findViewById(R.id.descArea);
        if (optDesc.isPresent()) {
            desc.setText(optDesc.get());
            desc.setEnabled(true);
        } else
            desc.setEnabled(false);

        TextView gestoreView = findViewById(R.id.gestArea);
        if (optGestore.isPresent()) {
            gestoreView.setText(String.format(Locale.getDefault(), getString(R.string.gestore), optGestore.get()));
            gestoreView.setEnabled(true);
        } else
            gestoreView.setEnabled(false);

        LinearLayout elencoTipi = findViewById(R.id.elencoTipi);
        elencoTipi.removeAllViews();
        TipoEmergenza[] tipi = area.getEmergenze();
        for (TipoEmergenza tipo : tipi) {
            TextView tipoView = (TextView) getLayoutInflater().inflate(R.layout.dettagli_text_view, elencoTipi, false);
            String tipoTradotto;
            try {
                tipoTradotto = getString(getResources().getIdentifier(tipo.getNome(), "string", getPackageName()));
            } catch (Exception e) {
                Log.e(TAG, "Errore nella traduzione del tipo" + tipo.toString(), e);
                tipoTradotto = tipo.getNome();
            }
            tipoView.setText(String.format(getString(R.string.tipoSupportato), tipoTradotto));
            elencoTipi.addView(tipoView);
        }

        LinearLayout elencoFoto = findViewById(R.id.elencoFoto);
        for (Fotografia foto : area.getFotografie()) {
            View view;
            if (foto.getTitolo().isPresent()) {
                TextView fotoView = (TextView) getLayoutInflater().inflate(R.layout.dettagli_text_view, elencoFoto, false);
                fotoView.setText(foto.getTitolo().get());
                fotoView.setCompoundDrawablesWithIntrinsicBounds(null, foto.getImmagine(), null, null);
                view = fotoView;

            } else {
                ImageView fotoView = new ImageView(this);
                fotoView.setImageDrawable(foto.getImmagine());
                view = fotoView;
            }
            elencoFoto.addView(view);
        }

        LinearLayout elencoExtra = findViewById(R.id.elencoExtra);
        elencoExtra.removeAllViews();
    }

    public void onAvviaNavigazione(View sorgenteClick) {
        viewModel.onAvviaNavigazione();
    }
}
