package it.dsantini.areediattesa.view;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Polygon;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.view.helper.GestionePermessi;
import it.dsantini.areediattesa.view.helper.impl.GoogleMapConfinePainter;
import it.dsantini.areediattesa.viewModel.GestioneViewModel;
import it.dsantini.areediattesa.viewModel.impl.GestioneViewModelBase;

public class GestioneView extends FragmentActivity implements OnMapReadyCallback, Observer<GruppoAree[]> {
    private static final String CLASS = "GestioneView";

    private GoogleMap map;
    private GestioneViewModel viewModel;

    private GruppoAree gruppoSelezionato = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final String TAG = CLASS + "::onCreate";

        setContentView(R.layout.activity_gestione_view);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mappaGestione);
        mapFragment.getMapAsync(this);

        viewModel = ViewModelProviders.of(this).get(GestioneViewModelBase.class);

        viewModel.getStato().observe(this, new StatoObserver());
    }

    private class StatoObserver implements Observer<Stato> {
        @Override
        public void onChanged(@Nullable Stato stato) {
            final String TAG = "StatoObserver::onChanged";

            if (stato == null)
                Log.w(TAG, "Stato nullo");
            else {
                try {
                    int id = getResources().getIdentifier(stato.name(), "string", getPackageName());
                    Snackbar.make(findViewById(R.id.gestione), getString(id), Snackbar.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.e(TAG, "Errore nella visualizzazione di " + stato.toString(), e);
                    Snackbar.make(findViewById(R.id.gestione), getString(R.string.possibile_errore_persistenza), Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        viewModel.getGruppi().observe(this, this);

        map.setOnPolygonClickListener(new GestioneOnPolygonClickListener());

        if (GestionePermessi.HoPermessoLocalizzazione(this))
            map.setMyLocationEnabled(true);
    }

    private class GestioneOnPolygonClickListener implements GoogleMap.OnPolygonClickListener {
        @Override
        public void onPolygonClick(Polygon polygon) {
            final String TAG = "GestioneOnPolygonClickListener::onPolygonClick";

            final Object tag = polygon.getTag();
            Log.d(TAG, "Click sul poligono " + tag);

            if (tag instanceof GruppoAree) {
                Log.v(TAG, "tag GruppoAree, ggiiornamento bottoni di gestione");
                final GruppoAree gruppo = (GruppoAree) tag;
                gruppoSelezionato = gruppo;
                findViewById(R.id.aggiungi_preferito).setVisibility(gruppo.isPreferito() ? View.GONE : View.VISIBLE);
                findViewById(R.id.rimuovi_preferito).setVisibility(gruppo.isPreferito() ? View.VISIBLE : View.GONE);
                findViewById(R.id.gestione_gruppo).setVisibility(View.VISIBLE);
            } else
                Log.w(TAG, "Tipo tag sconosciuto, non faccio nulla");
        }
    }

    public void onAggiorna(View sorgenteClick) {
        if (gruppoSelezionato == null)
            Log.e("GestioneView::onAggiorna", "gruppoSelezionato nullo");
        else
            viewModel.onAggiorna(gruppoSelezionato);
    }

    public void onAggiornaTutti(View sorgenteClick) {
        viewModel.onAggiornaTutti();
    }

    public void onRimuovi(View sorgenteClick) {
        if (gruppoSelezionato == null)
            Log.e("GestioneView::onRimuovi", "gruppoSelezionato nullo");
        else
            viewModel.onRimuovi(gruppoSelezionato);
    }

    public void onRimuoviTutti(View sorgenteClick) {
        viewModel.onRimuoviTutti();
    }

    public void onAggiungiPreferito(View sorgenteClick) {
        if (gruppoSelezionato == null)
            Log.e("GestioneView::onRimuovi", "gruppoSelezionato nullo");
        else
            viewModel.onAggiungiPreferito(gruppoSelezionato);
    }

    public void onRimuoviPreferito(View sorgenteClick) {
        if (gruppoSelezionato == null)
            Log.e("GestioneView::onRimuovi", "gruppoSelezionato nullo");
        else
            viewModel.onRimuoviPreferito(gruppoSelezionato);
    }

    @Override
    public void onChanged(@Nullable GruppoAree[] gruppi) {
        final String TAG = "GestioneView::onChanged";

        findViewById(R.id.gestione_gruppo).setVisibility(View.INVISIBLE);
        if (gruppi == null)
            Log.e(TAG, "Elenco gruppi nullo");
        else {
            Log.v(TAG, "Gruppi cambiati, gruppi.length=" + gruppi.length);

            map.clear();

            for (GruppoAree gruppo : gruppi)
                gruppo.getConfine().accept(new GoogleMapConfinePainter(map, gruppo));
        }
    }
}
