package it.dsantini.areediattesa.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.impl.googleMaps.BoundingBoxLatLngBounds;
import it.dsantini.areediattesa.view.helper.GestionePermessi;
import it.dsantini.areediattesa.view.helper.impl.GoogleMapAreaGeometriaPainter;
import it.dsantini.areediattesa.viewModel.StatoDownload;
import it.dsantini.areediattesa.viewModel.impl.MappaViewModelBase;

//import com.jakewharton.threetenabp.AndroidThreeTen;

public class MappaView extends FragmentActivity implements OnMapReadyCallback, Observer<AreaEmergenza[]> {
    private static final String COARSE_LOC = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String FINE_LOC = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final int PERMISSION_ID = 42;

    private MappaViewModelBase viewModel;
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //if (Build.VERSION.SDK_INT < 26)
        //    AndroidThreeTen.init(this);

        final String TAG = "MappaView::onCreate";

        if (getString(R.string.google_maps_key).equals("API_KEY")) {
            String error = getString(R.string.gmaps_api_key_error);
            Log.e(TAG, error);
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }

        setContentView(R.layout.activity_mappa_view);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mappaAree);
        mapFragment.getMapAsync(this);

        viewModel = ViewModelProviders.of(this).get(MappaViewModelBase.class);

        viewModel.getStato().observe(this, new Observer<Stato>() {
            @Override
            public void onChanged(@Nullable Stato stato) {
                if (stato == null)
                    Log.w(TAG, "Stato nullo");
                else {
                    try {
                        int id = getResources().getIdentifier(stato.name(), "string", getPackageName());
                        Snackbar.make(findViewById(R.id.aree), getString(id), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Log.e(TAG, "Errore nella visualizzazione di " + stato.toString(), e);
                        Snackbar.make(findViewById(R.id.aree), getString(R.string.possibile_errore_persistenza), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });
        viewModel.getStatoDownload().observe(this, new Observer<StatoDownload>() {
            @Override
            public void onChanged(@Nullable StatoDownload statoDownload) {
                if (statoDownload == null)
                    Log.w(TAG, "Stato download nullo");
                else {
                    try {
                        int id = getResources().getIdentifier(statoDownload.name(), "string", getPackageName());
                        Snackbar.make(findViewById(R.id.aree), getString(id), Snackbar.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Log.e(TAG, "Errore nella visualizzazione di " + statoDownload.toString(), e);
                        Snackbar.make(findViewById(R.id.aree), getString(R.string.possibile_errore_rete), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        TextView v = findViewById(R.id.attribuzione);
        v.setMovementMethod(LinkMovementMethod.getInstance()); // Rende il link cliccabile

        checkNavigazione();
    }

    private void checkNavigazione() {
        String query = "google.navigation:q=0.0,0.0";
        Uri uri = Uri.parse(query);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        if (intent.resolveActivity(getPackageManager()) == null) // Nessuna app disponibile per la navigazione
            Snackbar.make(findViewById(R.id.aree), getString(R.string.navigazione_nessuna_app), Snackbar.LENGTH_LONG).show();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final String TAG = "MappaView::onMapReady";

        this.map = googleMap;

        viewModel.getAree().observe(this, this);

        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLngBounds bounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
                try {
                    Confine porzione = new BoundingBoxLatLngBounds(bounds, null);
                    viewModel.onSposta(porzione);
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, "Errore nella reazione allo spostamento della mappa", e);
                    Snackbar.make(findViewById(R.id.mappaAree), "Errore nel caricamento delle aree", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return onGeometriaClick(marker.getTag());
            }
        });

        googleMap.setOnPolygonClickListener(new GoogleMap.OnPolygonClickListener() {
            @Override
            public void onPolygonClick(Polygon polygon) {
                onGeometriaClick(polygon.getTag());
            }
        });

        /*
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                Object tag = marker.getTag();
                if (tag == null)
                    Log.e(TAG, "Tag non presente");
                else if (!(tag instanceof AreaEmergenza))
                    Log.e(TAG, "Tag non istanza di AreaEmergenza");
                else
                    viewModel.onScegliArea((AreaEmergenza) tag);
            }
        });
        */


        if (GestionePermessi.HoPermessoLocalizzazione(this))
            map.setMyLocationEnabled(true);
        else
            ActivityCompat.requestPermissions(this, new String[]{COARSE_LOC, FINE_LOC}, PERMISSION_ID);

    }

    public boolean onGeometriaClick(Object tag) {
        final String TAG = "MappaView::onGeometriaClick";
        boolean completato = false;

        if (tag == null)
            Log.e(TAG, "Tag non presente");
        else if (!(tag instanceof AreaEmergenza))
            Log.e(TAG, "Tag non istanza di AreaEmergenza");
        else {
            viewModel.onScegliArea((AreaEmergenza) tag);
            completato = true;
        }

        return completato;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int code, @NonNull String[] permessi, @NonNull int[] risultati) {
        final String TAG = "onRequestPermissionsRes";

        switch (code) {
            case PERMISSION_ID:
                if (risultati[0] == PackageManager.PERMISSION_GRANTED
                        || risultati[1] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Permesso localizzazione concesso");
                    map.setMyLocationEnabled(true);
                } else
                    Log.w(TAG, "Permesso localizzazione negato");
                break;

            default:
                Log.w(TAG, "Codice richiesta di permessi sconosciuto");
        }
    }

    @Override
    public void onChanged(@Nullable AreaEmergenza[] aree) {
        final String TAG = "MappaView::onChanged";

        if (aree == null)
            Log.w(TAG, "Aree nullo");
        else {
            Log.v(TAG, "Aree cambiate, aree.length=" + aree.length);

            // Rimuovi le geometrie precedenti
            map.clear();

            // Aggiungi le nuove geometrie
            for (AreaEmergenza area : aree)
                area.getGeometria().accept(new GoogleMapAreaGeometriaPainter(map, area));
        }
    }

    public void onGestioneGruppi(View sorgenteClick) {
        Intent intent = new Intent(this, GestioneView.class);
        Log.v("MappaView::onGestioneGruppi", "-------------------- PASSAGGIO ALLA GESTIONE DEI GRUPPI --------------------");
        getApplication().startActivity(intent);
    }
}
