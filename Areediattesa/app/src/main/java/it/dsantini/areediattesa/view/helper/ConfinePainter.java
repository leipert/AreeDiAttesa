package it.dsantini.areediattesa.view.helper;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.BoundingBox;

public interface ConfinePainter {
    void stampa(@NonNull BoundingBox b);
}
