package it.dsantini.areediattesa.view.helper;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class GestionePermessi {
    private static final String COARSE_LOC = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String FINE_LOC = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final int PERMISSION_ID = 42;

    public static boolean HoPermessoLocalizzazione(Context context) {
        final String TAG = "GestionePermessi::HoPermessoLocalizzazione";

        boolean coarse = ContextCompat.checkSelfPermission(context, COARSE_LOC) == PackageManager.PERMISSION_GRANTED,
                fine = ContextCompat.checkSelfPermission(context, FINE_LOC) == PackageManager.PERMISSION_GRANTED;

        Log.i(TAG, COARSE_LOC + (coarse ? " concesso" : " respinto"));
        Log.i(TAG, FINE_LOC + (fine ? " concesso" : " respinto"));

        return coarse || fine;
    }
}
