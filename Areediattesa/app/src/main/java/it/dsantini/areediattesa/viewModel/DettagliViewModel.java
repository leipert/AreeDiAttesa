package it.dsantini.areediattesa.viewModel;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.AreaEmergenza;

public interface DettagliViewModel {
    /***
     * @return Area di emergenza osservabile di cui mostrare i dettagli
     */
    @NonNull
    LiveData<AreaEmergenza> getArea();

    /***
     * @return Stato osservabile dei dettagli
     */
    @NonNull
    LiveData<StatoDettagli> getStato();

    /***
     * Event Handler per l'avvio della navigazione
     */
    void onAvviaNavigazione();
}
