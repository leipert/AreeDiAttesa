package it.dsantini.areediattesa.viewModel;

public enum StatoDettagli {
    pronto,
    navigazione_errore,
    navigazione_nessuna_app
}
