package it.dsantini.areediattesa.viewModel.impl;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import it.dsantini.areediattesa.data.MappaRepository;
import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.data.impl.MappaRepositoryBase;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.SorgenteAree;
import it.dsantini.areediattesa.rete.impl.SorgenteAreeOverpass;
import it.dsantini.areediattesa.viewModel.GestioneViewModel;
import it.dsantini.areediattesa.viewModel.StatoDownload;

public class GestioneViewModelBase extends ViewModel implements GestioneViewModel, Observer<Mappa> {
    private static final String TAG = "GestioneViewModelBase";

    @NonNull
    private final MappaRepository m;
    @NonNull
    private final MediatorLiveData<GruppoAree[]> gruppi;
    @NonNull
    private final MutableLiveData<StatoDownload> statoDownload;
    @NonNull
    private final SorgenteAree sorgente;

    public GestioneViewModelBase() {
        gruppi = new MediatorLiveData<>();
        m = MappaRepositoryBase.getInstance();
        sorgente = new SorgenteAreeOverpass();
        statoDownload = new MutableLiveData<>();
        statoDownload.setValue(StatoDownload.scaricato);

        gruppi.addSource(m.getMappa(), this);
    }

    @Override
    public void onChanged(@Nullable Mappa mappa) {
        final String TAG = "GestioneViewModelBase::onChanged(Mappa)";

        if (mappa == null) {
            Log.e(TAG, "Mappa nulla");
            gruppi.setValue(null);
        } else {
            Log.v(TAG,"Mappa cambiata, mappa.gruppi.length=" + mappa.getGruppi().length);
            gruppi.setValue(mappa.getGruppi());
        }
    }

    @NonNull
    @Override
    public LiveData<GruppoAree[]> getGruppi() {
        return gruppi;
    }

    @NonNull
    @Override
    public LiveData<Stato> getStato() {
        return m.getStato();
    }

    @NonNull
    @Override
    public LiveData<StatoDownload> getStatoDownload() {
        return statoDownload;
    }

    @Override
    public void onAggiornaTutti() {
        Mappa mappa = m.getMappa().getValue();
        if (mappa != null) {
            for (GruppoAree g : mappa.getGruppi()) {
                Confine c = g.getConfine();
                new DownloadAsync(statoDownload, sorgente, c, m).start();
            }
        }
    }

    @Override
    public void onAggiorna(@NonNull GruppoAree g) {
        if (g == null)
            throw new IllegalArgumentException("GruppoAree g nullo");

        Mappa mappa = m.getMappa().getValue();
        if (mappa != null) {
            Confine c = g.getConfine();
            statoDownload.setValue(StatoDownload.scaricamento_in_corso);
            try {
                GruppoAree nuovo = sorgente.download(c);
                statoDownload.setValue(StatoDownload.scaricato);
                Mappa nuova = mappa.senza(g).con(nuovo);
                m.setMappa(nuova);
            } catch (DownloadException e) {
                statoDownload.setValue(StatoDownload.scaricamento_fallito);
                Log.e(TAG, "Download", e);
            }
        }
    }

    @Override
    public void onRimuoviTutti() {
        Mappa mappa = m.getMappa().getValue();
        if (mappa != null) {
            for (GruppoAree g : mappa.getGruppi())
                if (!g.isPreferito())
                    mappa = mappa.senza(g);

            m.setMappa(mappa);
        }
    }

    @Override
    public void onRimuovi(@NonNull GruppoAree g) {
        if (g == null)
            throw new IllegalArgumentException("GruppoAree g nullo");

        Mappa mappa = m.getMappa().getValue();
        if (mappa != null) {
            Mappa nuova = mappa.senza(g);
            m.setMappa(nuova);
        }
    }

    @Override
    public void onAggiungiPreferito(@NonNull GruppoAree g) {
        if (g == null)
            throw new IllegalArgumentException("GruppoAree g nullo");

        if (!g.isPreferito()) {
            Mappa mappa = m.getMappa().getValue();
            GruppoAree nuovo = g.withPreferito(true);
            if (mappa != null) {
                Mappa nuova = mappa.senza(g).con(nuovo);
                m.setMappa(nuova);
            }
        }
    }

    @Override
    public void onRimuoviPreferito(@NonNull GruppoAree g) {
        if (g == null)
            throw new IllegalArgumentException("GruppoAree g nullo");

        if (g.isPreferito()) {
            Mappa mappa = m.getMappa().getValue();
            GruppoAree nuovo = g.withPreferito(false);
            if (mappa != null) {
                Mappa nuova = mappa.senza(g).con(nuovo);
                m.setMappa(nuova);
            }
        }
    }
}
