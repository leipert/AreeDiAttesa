package it.dsantini.areediattesa.model;

import org.junit.Test;

import java.time.LocalDateTime;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.model.impl.BoundingBoxBase;
import it.dsantini.areediattesa.model.impl.GruppoAreeBase;
import it.dsantini.areediattesa.model.impl.MappaBase;

public class MappaBaseTest {
    private final Confine confineA, confineB;
    private final GruppoAree gruppoA, gruppoB;
    private final GruppoAree[] gruppi;

    public MappaBaseTest() {
        confineA = new BoundingBoxBase(0, 1, 0, 1, "TestA");
        confineB = new BoundingBoxBase(1, 2, 1, 2, "TestB");
        gruppoA = new GruppoAreeBase(new AreaEmergenza[]{}, confineA, false, LocalDateTime.now());
        gruppoB = new GruppoAreeBase(new AreaEmergenza[]{}, confineB, false, LocalDateTime.now());
        gruppi = new GruppoAree[]{gruppoA, gruppoB};
    }

    @Test
    public void ok() {
        new MappaBase(gruppi);
    }

    @Test(expected = IllegalArgumentException.class)
    public void gruppiNull() {
        new MappaBase(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void gruppoNull() {
        new MappaBase(new GruppoAree[]{gruppoA, null});
    }

    @Test(expected = IllegalArgumentException.class)
    public void gruppoRipetizione() {
        new MappaBase(new GruppoAree[]{gruppoA, gruppoB, gruppoA});
    }
}
