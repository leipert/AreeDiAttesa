package it.dsantini.areediattesa.model;

import org.junit.Test;

import it.dsantini.areediattesa.model.impl.NodoBase;

public class NodoBaseTest {

    @Test
    public void posPos() {
        new NodoBase(10, 10);
    }

    @Test
    public void posNeg() {
        new NodoBase(10, -10);
    }

    @Test
    public void negPos() {
        new NodoBase(-10, 10);
    }

    @Test
    public void negNeg() {
        new NodoBase(-10, -10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void latBassa() {
        new NodoBase(-91, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void latAlta() {
        new NodoBase(91, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lonBassa() {
        new NodoBase(10, -181);
    }

    @Test(expected = IllegalArgumentException.class)
    public void lonAlta() {
        new NodoBase(10, 181);
    }
}
