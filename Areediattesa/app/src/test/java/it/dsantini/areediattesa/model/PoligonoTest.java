package it.dsantini.areediattesa.model;

import org.junit.Test;

import it.dsantini.areediattesa.model.impl.NodoBase;
import it.dsantini.areediattesa.model.impl.Poligono;

public class PoligonoTest {
    private final Nodo a, b, c, d;

    public PoligonoTest() {
        a = new NodoBase(0,0);
        b = new NodoBase(0,1);
        c = new NodoBase(1,1);
        d = new NodoBase(1,0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullo() {
        new Poligono(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void vuoto() {
        new Poligono(new Nodo[0]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void uno() {
        new Poligono(new Nodo[]{a});
    }

    @Test(expected = IllegalArgumentException.class)
    public void due() {
        new Poligono(new Nodo[]{a,b});
    }

    @Test
    public void tre() {
        new Poligono(new Nodo[]{a,b,c});
    }

    @Test
    public void quattro() {
        new Poligono(new Nodo[]{a,b,c,d});
    }

    @Test(expected = IllegalArgumentException.class)
    public void ripetuto() {
        new Poligono(new Nodo[]{a,b,c,a});
    }

    @Test(expected = IllegalArgumentException.class)
    public void nodoNullo() {
        new Poligono(new Nodo[]{a,b,c,null});
    }


}
