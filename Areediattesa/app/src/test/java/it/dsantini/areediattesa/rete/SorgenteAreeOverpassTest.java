package it.dsantini.areediattesa.rete;

import org.junit.Assert;
import org.junit.Test;

import it.dsantini.areediattesa.rete.SorgenteAree;
import it.dsantini.areediattesa.rete.impl.SorgenteAreeOverpass;

public class SorgenteAreeOverpassTest {
    @Test(expected = IllegalArgumentException.class)
    public void nullEndpoint() {
        SorgenteAree s = new SorgenteAreeOverpass(null);
    }

}
