package eu.danielesantini.areediattesa.async;

import android.os.AsyncTask;
import android.util.Log;

public class AsyncImageSetter extends AsyncTask<ImageResourceToSet,Void,ImageToSet> {
    private static final String TAG = "AsyncImageSetter";

    @Override
    protected ImageToSet doInBackground(ImageResourceToSet... imgs) {
        Log.d(TAG,"Caricamento asincrono immagine");
        return new ImageToSet(imgs[0].getView(), imgs[0].getResources().getDrawable(imgs[0].getImgId()));
    }

    @Override
    protected void onPostExecute(ImageToSet img){
        Log.d(TAG,"Inserimento immagine");
        img.getView().setImageDrawable(img.getDrawable());
    }
}
