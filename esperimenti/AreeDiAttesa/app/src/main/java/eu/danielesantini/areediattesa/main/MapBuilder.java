package eu.danielesantini.areediattesa.main;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.Source;

import org.json.JSONObject;

import java.util.Scanner;

import eu.danielesantini.areediattesa.R;
import eu.danielesantini.areediattesa.offline.OfflineDownloader;

public class MapBuilder implements OnMapReadyCallback {
    private Activity context;
    private View positionButton;

    private static final LatLngBounds BOUNDS = new LatLngBounds.Builder().include(new LatLng(44.2836, 11.8113)).include(new LatLng(44.5694, 11.5844)).build();
    private static final String TAG = "MapBuilder",
            ICON = "icon",
            SRC = "attesa",
            JSON_FIELD_REGION_NAME = "regione",
            JSON_CHARSET = "UTF-8";
    public static final String PUNTI = "punti";

    public MapBuilder(Activity context, View positionButton) {
        this.context = context;
        this.positionButton = positionButton;
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        // Ottieni punti di raccolta
        mapboxMap.addSource(getSource());

        // Carica icona punti di raccolta
        mapboxMap.addImage(ICON, getIcon());

        // Aggiungi i punti di raccolta alla mappa
        SymbolLayer puntiAttesa = new SymbolLayer(PUNTI, SRC).withProperties(PropertyFactory.iconImage(ICON));
        mapboxMap.addLayer(puntiAttesa);

        // Limite di navigazione
        mapboxMap.setLatLngBoundsForCameraTarget(BOUNDS);

        // Aggiancia l'handler per i click
        mapboxMap.addOnMapClickListener(new MapClickHandler(context, mapboxMap));

        positionButton.setOnClickListener(new FloatingClickHandler(context, mapboxMap));
    }

    private Source getSource() {
        Log.d(TAG, "getSource inizia");

        //Toast.makeText(context, R.string.loading, Toast.LENGTH_SHORT).show();

        GeoJsonSource src;
        //try {
        //    URL overpass = new URL("http", "overpass-api.de", "/api/interpreter?data=%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3B%0Aarea%283600043020%29-%3E.searchArea%3B%0A%28%0A%20%20node%5B%22emergency%punto22%3D%22assembly_point%punto22%5D%28area.searchArea%29%3B%0A%29%3B%0Aout%20body%3B%0A%3E%3B%0Aout%20skel%20qt%3B");
        //    src = new GeoJsonSource("attesa", overpass); // Errore: overpass API ha come output un json in formato osm, non un geojson
        //    Log.d("OnMapReadyCallback","Download geojson effettuato");
        //} catch (Exception e){
        //    Log.d("OnMapReadyCallback","Download geojson fallito, uso file locale");
        Scanner s = new Scanner(context.getResources().openRawResource(R.raw.attesa)).useDelimiter("\\A");
        Log.d(TAG, "Apertura geojson locale completata");
        String geojson = "";
        if (s.hasNext())
            geojson = s.next();
        else
            Log.e(TAG, ".geojson vuoto");
        Log.d(TAG, "Lettura geojson locale completata");
        Log.v(TAG, geojson);
        src = new GeoJsonSource(SRC, geojson);
        //}

        Log.d(TAG, "getSource return");
        return src;
    }

    private Bitmap getIcon() {
        Log.d(TAG, "getIcon return");
        return BitmapFactory.decodeResource(context.getResources(), R.drawable.attesa);
    }

    // Download offline mappa
    private void download(MapboxMap mapboxMap) {
        OfflineManager offlineManager = OfflineManager.getInstance(context);
        OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(mapboxMap.getStyleUrl(), BOUNDS, context.getResources().getInteger(R.integer.minZoom), context.getResources().getInteger(R.integer.maxZoom), context.getResources().getDisplayMetrics().density);
        byte[] metadata;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JSON_FIELD_REGION_NAME, "Imola");
            String json = jsonObject.toString();
            metadata = json.getBytes(JSON_CHARSET);
        } catch (Exception exception) {
            Log.e("download()", "Failed to encode metadata: " + exception.getMessage());
            metadata = null;
        }
        offlineManager.createOfflineRegion(definition, metadata, new OfflineDownloader());
    }
}
