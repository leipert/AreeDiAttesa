package it.dsantini.assemblypoints.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

public class TestB {
    private MutableLiveData<TestA[]> a;

    public TestB(){
        a = new MutableLiveData<>();
    }

    public LiveData<TestA[]> getA(){
        return a;
    }

    public void add(){
        Collection<TestA> c;
        if(a.getValue()==null)
            c=new ArrayList<>();
        else
            c = Arrays.asList(a.getValue());
        c.add(new TestA());
        a.setValue(c.toArray(new TestA[0]));
    }

}
