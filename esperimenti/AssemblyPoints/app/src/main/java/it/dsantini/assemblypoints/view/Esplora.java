package it.dsantini.assemblypoints.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

// https://www.mapbox.com/install/android/add/
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.location.OnCameraTrackingChangedListener;
import com.mapbox.mapboxsdk.maps.MapView;

// https://www.mapbox.com/help/android-navigation-sdk/#display-user-location
// classes needed to add the location component
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import android.location.Location;
import android.view.ViewGroup;
import android.widget.Toast;

import android.support.annotation.NonNull;

import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.mapboxsdk.maps.UiSettings;

import java.util.List;

import it.dsantini.assemblypoints.R;
import timber.log.Timber;


public class Esplora extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener {
    private MapView mapView;

    // variables for adding location layer
    private MapboxMap mapboxMap;
    private PermissionsManager permissionsManager;

    private static final String TAG = "NavigationActivty";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Timber.plant(new Timber.DebugTree());

        Timber.v("Setup Mapbox token...");
        Mapbox.getInstance(this, "pk.eyJ1IjoiZGFueXNhbiIsImEiOiJjamYyb3p0eXEwMTByMnhvMXRvNjNocHllIn0.UMRjf9BaF_2mg7JvDzNEqg");

        Timber.v("Setup layout...");
        setContentView(R.layout.activity_esplora);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        Timber.v("Setup MapView...");
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        Timber.v("Setup FAB...");
        findViewById(R.id.localization).setOnClickListener(view -> enableLocationComponent());

        Timber.v("onCreate() completato");
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_navigation, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        this.mapboxMap = mapboxMap;

        Timber.v("Setup visibilità FAB...");
        mapboxMap.getLocationComponent().addOnCameraTrackingChangedListener(new OnCameraTrackingChangedListener() {
            @Override
            public void onCameraTrackingDismissed() {

            }

            @Override
            public void onCameraTrackingChanged(int currentMode) {
                final int visibility = currentMode == CameraMode.TRACKING ? View.GONE : View.VISIBLE;
                Timber.v("CameraMode %d -> FAB visibility %d", currentMode, visibility);
                findViewById(R.id.localization).setVisibility(visibility);
            }
        });

        setCompassMarginTop(getStatusBarHeight() + 20);

        if (areLocationPermissionsGranted() && isGpsEnabled())
            showLocation();
    }

    private void setCompassMarginTop(int val) {
        Timber.v("Impostazione margine superiore bussola a %d", val);
        UiSettings s = mapboxMap.getUiSettings();
        s.setCompassMargins(s.getCompassMarginLeft(), val, s.getCompassMarginRight(), s.getCompassMarginBottom());
    }

    private int getStatusBarHeight() {
        Rect rect = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        Timber.v("Altezza status bar: %d", rect.top);
        return rect.top;
    }

    //@SuppressWarnings({"MissingPermission"})
    void enableLocationComponent() {
        Timber.v("Abilitazione Location Component...");
        // Check if permissions are enabled and if not request
        if (areLocationPermissionsGranted()) {
            if (!isGpsEnabled())
                askEnableGps();

            showLocation();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    private boolean areLocationPermissionsGranted() {
        boolean granted = PermissionsManager.areLocationPermissionsGranted(this);

        if (granted)
            Timber.v("L'autorizzazione alla posizione è disponibile");
        else
            Timber.v("L'autorizzazione alla posizione NON è disponibile");

        return granted;
    }

    private boolean isGpsEnabled() {
        boolean enabled;

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (manager == null)
            enabled = false;
        else
            enabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (enabled)
            Timber.v("Il GPS è abilitato");
        else
            Timber.v("Il GPS è disabilitato");

        return enabled;
    }

    private void askEnableGps() {
        Timber.v("Richiedo di attivare il GPS...");
        new AlertDialog.Builder(this).
                setMessage(R.string.askEnableGPS).
                setPositiveButton(R.string.yes, (dialog, id) -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))).
                setNegativeButton(R.string.no, (dialog, id) -> dialog.cancel()).
                create().
                show();
    }

    private void showLocation() {
        Timber.v("Visualizzazione posizione...");
        try {
            // Activate the MapboxMap LocationComponent to show user location
            // Adding in LocationComponentOptions is also an optional parameter
            final LocationComponent locationComponent = mapboxMap.getLocationComponent();
            locationComponent.activateLocationComponent(this);
            locationComponent.setLocationComponentEnabled(true);
            // Set the component's camera mode
            locationComponent.setCameraMode(CameraMode.TRACKING);
        } catch (SecurityException e) {
            Timber.e(e);
            Snackbar.make(mapView, R.string.permissionError, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, R.string.user_location_permission_explanation, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            Timber.v("Autorizzazione alla posizione ottenuta");
            enableLocationComponent();
        } else {
            Timber.e("Autorizzazione alla posizione rifiutata");
            Toast.makeText(this, R.string.user_location_permission_not_granted, Toast.LENGTH_LONG).show();
            //this.finish();
        }
    }
}
