GoogleMap map = ...
Area area = ...
Poligono p = (Poligono) area.getGeometria();
PolygonOptions pOpt = new PolygonOptions();
for(Nodo n : p.getVertici())
	pOpt.add(new LatLng(n.getLatitudine(), n.getLongitudine()));
Polygon gmp = map.addPolygon(pOpt);
gmp.setTag(area);

