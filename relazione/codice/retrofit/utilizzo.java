APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
Call<MultipleResource> call = apiInterface.doGetListResources();
call.enqueue(new Callback<MultipleResource>() {
	@Override
	public void onResponse(Call<MultipleResource> call, Response<MultipleResource> response) {
		Log.d("TAG",response.code()+"");

		String displayResponse = "";

		MultipleResource resource = response.body();
		Integer text = resource.page;
		Integer total = resource.total;
		Integer totalPages = resource.totalPages;
		List<MultipleResource.Datum> datumList = resource.data;

		// Utilizzo dei dati
	}

	@Override
	public void onFailure(Call<MultipleResource> call, Throwable t) {
		call.cancel();
	}
});
